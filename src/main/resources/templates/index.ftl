<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Decathlon points calculator</title>
    <link rel="stylesheet" type="text/css" href="/static/semantic_custom.min.css" />

    <script src="/static/jquery-3.2.1.min.js"></script>
    <script src="/static/underscore-1.8.3.min.js"></script>

    <script>
        $(document).ready(function() {
            $("#decaForm").find("input:first").focus();

            $("#clearForm").click(function(event) {
                $('#decaForm').find("input").val("");
                // recalculate
                $("#decaForm").submit();
            });

            $("#loadSampleData").click(function(event) {
                $.ajax({
                    type: "GET",
                    url: "/api/sample_data",
                    dataType: "json"
                }).done(function(responseData) {
                    var decaForm = $('#decaForm');
                    $.each(responseData, function(key, value){
                        decaForm.find('input[name='+key+']').val(value);
                    });
                    $("#decaForm").submit();
                }).fail(function(deferred) {
                    throw new Error("" + deferred.status + " " + deferred.statusText + " "+ deferred.responseText);
                });
            });

            $( "#decaForm" ).submit(function( event ) {
                // console.log("submit");
                event.preventDefault();

                var formData = $( this ).serializeArray();

                var d = {};
                $.each( formData, function( i, field ) {
                    d[field.name] = field.value;
                });

                $.ajax({
                    type: "POST",
                    url: "/api/calculate_points",
                    data: JSON.stringify(d),
                    dataType: "json"
                }).done(function(responseData) {
                    // console.log("done", responseData);

                    var decaForm = $('#decaForm');

                    decaForm.find("span.decaPoints").text("");
                    $.each(responseData.eventPoints, function(key, value){
                        decaForm.find('span#points_'+key).text(value);
                    });

                    decaForm.find('input').parent().removeClass("error");
                    $.each(responseData.inputErrors, function(key, value) {
                        decaForm.find('input[name='+key+']').parent().addClass("error");
                    });

                    decaForm.find('span#points_total').text(responseData.total);

                }).fail(function(deferred) {
                    throw new Error("" + deferred.status + " " + deferred.statusText + " "+ deferred.responseText);
                });
            });

            var lazySubmit = _.debounce(function() {
                $("#decaForm").submit();
            }, 100);

            $("#decaForm").find("input").on('change input', function() {
                lazySubmit();
            });
        });
    </script>

    <style>
        td.noPadding input {
            padding: 5px !important;
            margin-left: 2px !important;
            margin-right: 2px !important;
        }

        /* .ui.compact.table td */
        tfoot tr th  {
            padding-top: 0.25em !important;
            padding-bottom: 0.25em !important;
        }
        thead tr th {
            padding-top: 0.5em !important;
            padding-bottom: 0.5em !important;
        }

        .noPadding {
            padding: 0 !important;
        }
        .noBorder {
            border: 0 !important;
        }

        .fixedWidth {
            max-width: 400px !important;
            min-width: 400px !important;
        }
    </style>
</head>
<body>



<div class="ui two column centered grid">
    <div class="column fixedWidth">

    <br />
    <h3 class="ui header">Decathlon points calculator</h3>
    <br />

    <table class="ui table unstackable noBorder">
        <tr>
            <td>
                <div class="ui right aligned grid">
                    <div class="column noPadding">
                        <div id="clearForm" class="ui button compact basic negative">
                            Clear form
                        </div>

                        <div id="loadSampleData" class="ui button compact basic primary">
                            Load sample data
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>


    <form id="decaForm">

    <table class="ui definition celled selectable fixed compact table unstackable">
        <thead>
         <tr>
             <th class="one wide compact"></th>
             <th class="one wide compact">Result</th>
             <th class="one wide compact">Points</th>
         </tr>
        </thead>

        <tbody>
            <#list context.decaEvents as decaEvent>
                <tr>
                    <td class="right aligned">
                        ${decaEvent.getEventName()}
                    </td>
                    <td class="noPadding">
                        <div class="ui fluid input">
                            <input name="${decaEvent.name()}" type="text"
                                   placeholder="e.g. ${context.placeholders[decaEvent.name()]}"
                            >
                        </div>
                    </td>
                    <td>
                        <span class="decaPoints" id="points_${decaEvent.name()}"></span>
                    </td>
                </tr>
            </#list>
        </tbody>

        <tfoot>
            <tr>
                <th></th>
                <th class="ui center aligned">
                    <button class="ui primary button compact" type="submit">Calculate</button>
                </th>
                <th><span class="decaPoints" id="points_total"></span></th>

            </tr>
        </tfoot>

    </table>

    </form>

    </div>
</div>


</body>
</html>
