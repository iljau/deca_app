package iljau.decaapp;

public class ValidationError extends Error {
    private ErrorType errorType;

    public ValidationError(ErrorType errorType) {
        super(errorType.name());

        this.errorType = errorType;
    }

    public ErrorType getErrorType() {
        return errorType;
    }
}
