package iljau.decaapp

// Calculations based on:
// IAAF Scoring Tables for Combined Events, p. 24.
enum class EventType {
    TRACK,
    JUMP,
    THROW
}

enum class DecaEvent(val eventName: String, val eventType: EventType) {
    RUN_100M("100 m", EventType.TRACK),
    LONG_JUMP("Long jump", EventType.JUMP),
    SHOT_PUT("Shot put", EventType.THROW),
    HIGH_JUMP("High jump", EventType.JUMP),
    RUN_400M("400 m", EventType.TRACK),
    RUN_110M_HURDLES("110 m hurdles", EventType.TRACK),
    DISCUS_THROW("Discus throw", EventType.THROW),
    POLE_VAULT("Pole vault", EventType.JUMP),
    JAVELIN_THROW("Javelin throw", EventType.THROW),
    RUN_1500M("1500 m", EventType.TRACK),
}

data class CalculationResult(val points: Int?, val errorType: ErrorType?) {
    fun isValid(): Boolean {
        return errorType == null;
    }
}

data class DecaPoints (
        val event: DecaEvent, val A: Double, val B: Double, val C: Double
) {
    fun calculatePoints(result: String): CalculationResult {
        try {
            val res = _calculatePoints(result);
            return CalculationResult(res, null);
        } catch (e: ValidationError) {
            return CalculationResult(null, e.errorType);
        }
        catch (e: Exception) {
            return CalculationResult(null, ErrorType.INVALID);
        }
    }

    fun _calculatePoints(result: String): Int {
        if (result.trim().length == 0) {
            throw ValidationError(ErrorType.EMPTY);
        }

        val P: Double;
        if (event.eventType == EventType.TRACK) {
            // '4:17.52
            val splitRes = result.split(":", limit = 2);

            val minutes: Double;
            val seconds: Double;

            if (splitRes.size == 1) {
                val secondsStr = splitRes[0];
                minutes = 0.0
                seconds = secondsStr.toDouble();
            } else if (splitRes.size == 2) {
                val (minutesStr, secondsStr) = splitRes
                minutes = minutesStr.toDouble();
                seconds = secondsStr.toDouble();
            } else {
                throw ValidationError(ErrorType.INVALID);
            }

            if (seconds > 60) {
                throw ValidationError(ErrorType.INVALID);
            }

            P = minutes * 60 + seconds;

            return (A * Math.pow(B - P, C)).toInt();
        } else if (event.eventType == EventType.THROW) {
            P = result.toDouble();
            return (A * Math.pow(P - B, C)).toInt()
        } else if (event.eventType == EventType.JUMP) {
            P = result.toDouble() * 100;
            return (A * Math.pow(P - B, C)).toInt();
        }

        throw AssertionError("unknown eventType: ${event.eventType}")
    }
}