package iljau.decaapp;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.TemplateHandler;
import io.vertx.ext.web.templ.FreeMarkerTemplateEngine;
import io.vertx.ext.web.templ.TemplateEngine;

import java.util.LinkedHashMap;
import java.util.Map;

public class DecaApp extends AbstractVerticle {
    public void index(RoutingContext rc) {
        rc.put("decaEvents", DecaEvent.values());

        Map<String, String> placeholders = new LinkedHashMap<>();
        DecaHelpers.getSampleData().forEach( (key, value) -> {
            placeholders.put(key.name(), value);
        });

        rc.put("placeholders", placeholders);
        // render template
        rc.next();
    }

    public void sampleData(RoutingContext rc) {
        Map<DecaEvent, String> m = DecaHelpers.getSampleData();

        rc.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(m));
    }

    public void calculatePoints(RoutingContext rc) {
        JsonObject jsonObject = rc.getBodyAsJson();
        Map<String, Object> postData = jsonObject.getMap();

        Map<DecaEvent, DecaPoints> decaPointsMap = DecaHelpers.getDecaCalcTable();

        Map<DecaEvent, Integer> eventPoints = new LinkedHashMap<>();
        Map<DecaEvent, ErrorType> inputErrors = new LinkedHashMap<>();

        Integer total = 0;

        for(Map.Entry<String, Object> entry : postData.entrySet()) {
            String key = entry.getKey();
            Object val = entry.getValue();

            DecaEvent decaEvent;
            try {
                decaEvent = DecaEvent.valueOf(key);
            } catch (IllegalArgumentException e) {
                continue;
            }

            DecaPoints decaPoints = decaPointsMap.get(decaEvent);
            if (decaPoints == null) {
                continue;
            }

            CalculationResult calculationResult = decaPoints.calculatePoints(String.valueOf(val));
            if (calculationResult.isValid()) {
                eventPoints.put(decaEvent, calculationResult.getPoints());
                total += calculationResult.getPoints();
            } else {
                if (calculationResult.getErrorType() != ErrorType.EMPTY) {
                    inputErrors.put(decaEvent, calculationResult.getErrorType());
                }
            }
        }

        Map<String, Object> ret = new LinkedHashMap<>();
        ret.put("eventPoints", eventPoints);
        ret.put("inputErrors", inputErrors);
        if (!eventPoints.isEmpty()) {
            ret.put("total", total);
        } else {
            ret.put("total", null);
        }

        rc.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(ret));
    }

    public void favicon(RoutingContext rc) {
        rc.response().setStatusCode(204).end();
    }

    public void start() {
        Integer port = 8080;

        System.setProperty("io.vertx.ext.web.TemplateEngine.disableCache", "true");
        TemplateEngine templateEngine = FreeMarkerTemplateEngine.create();
        TemplateHandler templateHandler = TemplateHandler.create(templateEngine);

        Router router = Router.router(vertx);

        router.route().handler(BodyHandler.create());

        router.get("/favicon.ico").handler(this::favicon);

        router.get("/").handler(this::index);
        router.get("/").handler(templateHandler);

        router.post("/api/calculate_points").handler(this::calculatePoints);

        router.get("/api/sample_data").handler(this::sampleData);

        router.route("/static/*").handler(
                StaticHandler
                        .create("static")
                        .setCachingEnabled(false)
        );

//        router.route().failureHandler(ErrorHandler.create());

        HttpServer server = vertx.createHttpServer();
        server.requestHandler(router::accept).listen(port, httpServerAsyncResult -> {
            System.out.println("Starting server: http://localhost:" + port);

            if (httpServerAsyncResult.succeeded()) {
                System.out.println("Started!");
            } else {
                throw new Error(httpServerAsyncResult.cause());
            }
        });
    }
}
