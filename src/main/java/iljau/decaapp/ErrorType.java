package iljau.decaapp;

// validation error type
public enum ErrorType {
    INVALID,
    EMPTY
}
