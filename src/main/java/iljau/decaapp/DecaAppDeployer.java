package iljau.decaapp;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

public class DecaAppDeployer {
    public static void main(String args[]) {

        VertxOptions vertxOptions = new VertxOptions();
        vertxOptions.setFileResolverCachingEnabled(false);
        // to allow evaluating breakpoints indefinitely while debugging
        //vertxOptions.setMaxEventLoopExecuteTime(Long.MAX_VALUE);
        Vertx vertx = Vertx.vertx(vertxOptions);

        DecaApp verticle = new DecaApp();
        vertx.deployVerticle(verticle, res -> {
            if (res.succeeded()) {
                System.out.println("Deployment id is: " + res.result());
            } else {
                System.out.println("Deployment failed!");
                throw new Error(res.cause());
            }
        });
    }
}
