package iljau.decaapp;

import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class DecaHelpers {
    public static Map<DecaEvent, String> getSampleData() {
        Map<DecaEvent, String> m = new LinkedHashMap<>();
        m.put(DecaEvent.RUN_100M, "10.23");
        m.put(DecaEvent.LONG_JUMP, "7.88");
        m.put(DecaEvent.SHOT_PUT, "14.52");
        m.put(DecaEvent.HIGH_JUMP, "2.01");
        m.put(DecaEvent.RUN_400M, "45.00");
        m.put(DecaEvent.RUN_110M_HURDLES, "13.69");
        m.put(DecaEvent.DISCUS_THROW, "43.34");
        m.put(DecaEvent.POLE_VAULT, "5.20");
        m.put(DecaEvent.JAVELIN_THROW, "63.63");
        m.put(DecaEvent.RUN_1500M, "4:17.52");
        return m;
    }

    public static HashMap<DecaEvent, DecaPoints> getDecaCalcTable() {
        HashMap<String, DecaEvent> m = new HashMap<>();
        for(DecaEvent decaEvent : DecaEvent.values()) {
            m.put(decaEvent.getEventName(), decaEvent);
        }

        HashMap<DecaEvent, DecaPoints> ret = new LinkedHashMap<>();

        InputStream is = DecaHelpers.class.getResourceAsStream("/points.txt");
        Scanner scanner = new Scanner(is);
        while (scanner.hasNextLine()) {
            String[] cols = scanner.nextLine().split("\t");

            DecaPoints decaPoints = new DecaPoints(
                    m.get(cols[0]),
                    Double.valueOf(cols[1]),
                    Double.valueOf(cols[2]),
                    Double.valueOf(cols[3])
            );

            ret.put(decaPoints.getEvent(), decaPoints);
        }

        return ret;
    }
}
